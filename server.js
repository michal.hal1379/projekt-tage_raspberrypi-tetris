//Tetris server
let {start} = require('./socketServer');
start(8080);

const host = '0.0.0.0';
const port = 80;
const http = require('http');
const fs = require('fs');
const url = require('url');

let contentTypes = {
	'html':'text/html',
	'js':'application/javascript',
	'ico':'image/x-ico',
	'png':'image/png',
	'json':'application/json',
	'css': 'text/css',
}

let scores = [];
for(let i = 0; i< 10; i++) {
	scores.push({"score":0});
}

const requestListener = function(req, resp) {
	console.log(req.method + " " + req.url);
	//console.log(req.url);
	const queryObject = url.parse(req.url, true).query;
	req.url = url.parse(req.url, true).pathname;
	if(req.url.length >= 2 && req.url.slice(-1) == '/') req.url = req.url.slice(0,-1);	//remove / at end of url
	try {
		if(req.method == 'GET') {
			if(req.url == '/api/score') {
				resp.writeHead(200, {'Content-Type':'application/json'});
				resp.write(JSON.stringify(scores.sort((a,b) => b.score -a.score).slice(0,10)));
				resp.end();
			} else if(req.url == '/api/topscore') {
				resp.writeHead(200, {'Content-Type':'text/plain'});
				resp.write(Math.max(scores.map(score => score.score)));
				resp.end();
			}
			else {
				sendStaticContent(req, resp);
			}
		} else if (req.method == 'POST') {
			if(req.url == '/api/score') {
				scores.push(queryObject);
				resp.writeHead(200, {'Content-Type':'text/plain'});
				resp.write("data pushed");
				resp.end();
			} else {
				resp.writeHead(404, {'Content-Type':'text/html'});
				resp.write("not found");
				resp.end();
			}
		} else {
			resp.writeHead(501, {'Content-Type':'text/plain'});
			resp.write("not implemented" + e.message);
			resp.end();
		}
	} catch(e) {
		console.log(e.message);
		resp.writeHead(500, {'Content-Type':'text/plain'});
		resp.write("internal server error:" + e.message);
		resp.end();
	}
};

function sendStaticContent(req, resp) {
	if(req.url == '/' || req.url == '/index' || req.url == '/index.html') {
		req.url = '/index/index.html'
	}
	else if(/^\/(controller\/)?[0-9]+$/.test(req.url)) {
		req.url = '/controller/index.html';
	} else if(req.url == '/game') {
		req.url = '/game/index.html';
	} else if(req.url == '/singleplayer') {
		req.url = '/singleplayer/index.html';
	}
	req.url = './webApp' + req.url;
	if(fs.existsSync(req.url) && fs.lstatSync(req.url).isFile() ) {
		let contentType = contentTypes[req.url.split('.')[2]]
		resp.writeHead(200, {'Content-Type':contentType});
		let data = fs.readFileSync(req.url);
		resp.write(data);
		resp.end();
	} else {
		resp.writeHead(404, {'Content-Type':'text/html'});
		let data = fs.readFileSync('./webApp/index/index.html');
		resp.write(data);
		resp.end();
	}
}



const server = http.createServer(requestListener);

server.listen(port, host, () => {
		console.log("Server is running on "+host+":"+port);
		

});