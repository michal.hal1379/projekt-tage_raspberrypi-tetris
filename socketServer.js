const WebSocketServer = require('ws');
const bufferListeners = {};

function start(port) {
// Creating a new websocket server
const wss = new WebSocketServer.Server({ port: port })
wss.on("connection", (ws, req) => {
    console.log('socket:' + req.url);
    if(req.url == '/api/buffer/listen'){
        let gameId;
        do {
            gameId = Math.floor(Math.random()*9000)+1000;
        } while(bufferListeners[gameId])
        bufferListeners[gameId] = ws;
        ws.send(gameId);
        ws.on("close", () => {
            bufferListeners[gameId] == undefined;
        });
    } else if(/\/api\/buffer\/send\/[0-9]+/.test(req.url)) {
        let gameId = req.url.replace(/[^0-9]+/,'');
        ws.on("message", data => {
            sendBufferInfo(data.toString(), gameId);
        });
    }
});
}

function sendBufferInfo(message, gameId) {
    if(bufferListeners[gameId])
        bufferListeners[gameId].send(message);
}

module.exports = {start};