let socket = new WebSocket('ws://' + window.location.host + ':8080/api/buffer/send/' + window.location.pathname.replace(/[^0-9]+/,''));

function sendRL(){
	sendState("RL");
}

function sendRR(){
	sendState("RR");
}

function sendML(){
	sendState("ML");
}

function sendMR(){
	sendState("MR");
}

function sendGO(){
	sendState("GO");
}

function sendState(mvmnt){
	socket.send(mvmnt);
}
