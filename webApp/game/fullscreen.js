let fullScreen = false;
window.addEventListener("keydown", function (event) {
	if (event.defaultPrevented) {
	  return; // Do nothing if the event was already processed
	}
	//console.log(event.key);
	switch (event.key) {
	  case "f":
		if(fullScreen) {
            fullScreen = false;
            exitFullscreen();
        }
        else {
            fullScreen = true;
            enterFullscreen(this.document.documentElement);
        }
	}
  
	event.preventDefault();
  }, true);


function enterFullscreen(element) {
    if(element.requestFullscreen) {
      element.requestFullscreen();
    } else if(element.msRequestFullscreen) {      // for IE11 (remove June 15, 2022)
      element.msRequestFullscreen();
    } else if(element.webkitRequestFullscreen) {  // iOS Safari
      element.webkitRequestFullscreen();
    }
  }

  
function exitFullscreen() {
if(document.exitFullscreen) {
    document.exitFullscreen();
} else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
}
}