document.addEventListener("DOMContentLoaded", addEventListeners, true);

function addEventListeners() {
    let connectButton = document.getElementById("connectButton");
    connectButton.addEventListener("click", () => {
        let gameId = document.getElementById("inputGameId").value;
        window.location.href = "/" + gameId;
    });
    let hostButton = document.getElementById("hostButton");
    hostButton.addEventListener("click", () => {
        window.location.href = "/game";
    });
    let singleplayerButton = document.getElementById("singleplayerButton");
    singleplayerButton.addEventListener("click", () => {
        window.location.href = "/singleplayer";
    });
}
