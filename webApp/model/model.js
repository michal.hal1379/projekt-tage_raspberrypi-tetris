const X_COO = 0;
const Y_COO = 1;
const ROTATE_INDEX = "1";

let model = [
{"type":"L","color":"blue","0":[0,0], "1":[0,1], "2":[0,2], "3": [1,2]},
{"type":"Z","color":"green","0":[0,0], "1":[1,1], "2":[1,0], "3": [2,1]},
{"type":"RL","color":"orange","0":[0,0], "1":[1,0], "2":[2,0], "3": [2,1]},
{"type":"RZ","color":"red","0":[1,0], "1":[1,1], "2":[2,0], "3": [0,1]},
{"type":"Line","color":"deepskyblue", "0":[0,0], "1":[1,0], "2":[2,0], "3": [3,0]},
{"type":"Block","color":"yellow", "0":[0,0], "1":[1,0], "2":[0,1], "3": [1,1]},
{"type":"T","color":"magenta","0":[0,0], "1":[1,0], "2":[2,0], "3": [1,1]}]

function getRandomModel(){
    return deepCopy(model[Math.floor(Math.random()*7)]);
}

function deepCopy(obj) {
    let copy = {};
    for(let k in obj) {
        if(typeof(obj[k]) == 'object') {
            copy[k] = deepCopy(obj[k]);
        } else {
            copy[k] = obj[k];
        }
    }
    return copy;
}