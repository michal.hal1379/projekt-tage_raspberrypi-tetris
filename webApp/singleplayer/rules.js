
document.addEventListener("DOMContentLoaded", init, true);
window.addEventListener("keydown", function (event) {
	if (event.defaultPrevented) {
	  return; // Do nothing if the event was already processed
	}
	if(!running) {
		play();
	} else {
		switch (event.key) {
			case "ArrowDown":
			  inpt_client('GO');
			  break;
			case "ArrowUp":
			  inpt_client('RR');
			  break;
			case "ArrowLeft":
			  inpt_client('ML');
			  break;
			case "ArrowRight":
			  inpt_client('MR');
			  break;
			default:
			  return;
		  }
	}	
  
	event.preventDefault();
  }, true);

const CUBE_SIZE = "10px";
const CUBE_AMOUNT_X = 8;
const CUBE_AMOUNT_Y = 15;
const FREE_COLOR = "rgb(170, 170, 170)";
const CUBE_MOVING_COLOR = "blue";


const KEYS = {move_left: "ML", move_right: "MR", rotate_left: "RL",rotate_right:"RR",enter: "GO"};
let currentModel;
let tableModel = [];
let STANDARD_DELAY = 500;
let delayTime;
let MARGIN_SPAWN_X = 3;
let stats;
let MARGIN_SPAWN_Y = -2;
let running = false;
function init() {
	createTable();
	startGame();
}
function startGame(){
	getScoresFromServer();
	stats = {"score":0,"blocks":4,"round":1,"linesRemoved":0};
}

async function play(){
clearTable();
running = true;
//document.getElementById("info").style.backgroundColor = "rgb(245, 227, 93)";
document.getElementById("menu").style.display = "none";
currentModel = getRandomModel();
move(MARGIN_SPAWN_X,MARGIN_SPAWN_Y);
delayTime = STANDARD_DELAY;

while(running){
	drawMap();
	await new Promise(r => setTimeout(r, delayTime));
	if(check(0, 1)){
		move(0,1);
	}else{
		stats.blocks+=4;
		delayTime = calcDelay();
		recopy();
		currentModel = getRandomModel();
		move(MARGIN_SPAWN_X,MARGIN_SPAWN_Y);
	}
	checkIfRock();
	updateStats();
}

	//document.getElementById("info").style.backgroundColor = "rgb(32, 138, 138)";
	console.log("Game over");
	document.getElementById("currScore").innerHTML = "Your score is:"+stats.score;
	document.getElementById("menu").style.display = "block";
	if(stats.score != 0)
	{
		sendDataToServer('/api/score/?score=' + stats.score);
	}
	startGame();
}

function checkIfRock(){

	for(let line = 0;line<CUBE_AMOUNT_Y;line++){
		let success = true;
		for(let row = 0;row<CUBE_AMOUNT_X&&success;row++){
			if(tableModel[line][row].free){
				success = false;
			}
		}
		if(success){
			removeLine(line);
			stats.score+=CUBE_AMOUNT_X;
			stats.linesRemoved++;
			if(stats.linesRemoved%3 == 0) {
				stats.round++;
				delayTime = calcDelay();
			}
		}
	}
}

function removeLine(y){
	for(let row = y; row > 0; row--) {
		tableModel[row].forEach((cube, idx) => {cube.free = tableModel[row-1][idx].free; 
												cube.td.style.backgroundColor = tableModel[row-1][idx].td.style.backgroundColor;})
	}
	tableModel[0].forEach((cube, idx) => {cube.free = true; 
		cube.td.style.backgroundColor = FREE_COLOR;})
}

function updateStats(){
	document.getElementById("Score").innerHTML = stats["score"];
	document.getElementById("Blocks").innerHTML = stats["blocks"];
	document.getElementById("Round").innerHTML = stats["round"];
}

function drawMap(){
	cls();
	drawMovingCube();
}

function recopy(){
	for(let i in currentModel){
		if(typeof(currentModel[i]) == 'object'){
			let x_now = (currentModel[i])[X_COO];
			let y_now = (currentModel[i])[Y_COO];
			if(y_now < 0) {
				running = false;
			} else {
				tableModel[y_now][x_now].free = false;
				tableModel[y_now][x_now].td.style.backgroundColor = currentModel.color;
			}
		}
	}
}	


function move(x, y){
	if(check(x,y)){
		for(let i in currentModel){
			(currentModel[i])[X_COO] += x;
			(currentModel[i])[Y_COO] += y;
		}
	}
}

function check(x, y){
	let availableMove = true;
	
	for(let i in currentModel){
		if(typeof(currentModel[i]) == 'object'){
			let x_now = (currentModel[i])[X_COO] + x;
			let y_now = (currentModel[i])[Y_COO] + y;
			if(y_now>=0){
				if(x_now+(y_now*CUBE_AMOUNT_X)>=CUBE_AMOUNT_X*CUBE_AMOUNT_Y){
					availableMove = false;
					//console.log("ON GROUND");
				}else if(x_now < 0 || x_now > CUBE_AMOUNT_X-1){
					availableMove = false;
					//console.log("OUT OF MAP");
				}else if(!tableModel[y_now][x_now].free){
					availableMove = false;
					//console.log("TOUCH");
				}
			}
			
		}
	}
	return availableMove;
}

function cls(){
	tableModel.forEach(row => row.forEach(cube => {if(cube.free) cube.td.style.backgroundColor = FREE_COLOR;}));
}

function drawMovingCube(){
	for(let i in currentModel){
		if(typeof(currentModel[i]) == 'object' && (currentModel[i])[Y_COO] >= 0) {
			tableModel[(currentModel[i])[Y_COO]][(currentModel[i])[X_COO]].td.style.backgroundColor = currentModel.color;
		}
	}
}

function clearTable() {
	tableModel.forEach(row => row.forEach(cube => {cube.free = true;cube.td.style.backgroundColor = FREE_COLOR}))
}


function createTable(){
	let table = document.createElement("table");
	let td_size = window.outerHeight*devicePixelRatio/CUBE_AMOUNT_Y * 0.45;
	tableModel = [];

	table.classList.add("table");
	
	for(let y = 0; y< CUBE_AMOUNT_Y;y++){
		tableModel.push([]);
		let tr = table.insertRow();
		for(let x=0;x<CUBE_AMOUNT_X;x++){
			let td = tr.insertCell();
			td.setAttribute("x", x);
			td.setAttribute("y", y);
			td.style.height = td_size + "px";
			td.style.width = td_size + "px";
			tableModel[y].push({"free":true,"td":td});
		}
	}
	document.getElementById("board").appendChild(table);
}

function inpt_client(mv){
	if(mv == "ML"){
		if(check(-1,0))
			move(-1,0);
	}else if(mv == "MR"){
		if(check(1,0))
			move(1,0);
	}else if(mv == "RL"){
		if(checkRotate(-1))
			rotate(-1);
	}else if(mv == "RR"){
		if(checkRotate(1))
			rotate(1);
	}else if(mv == "GO"){
		delayTime = 20;
	}
	try{
		drawMap();
	}catch(e){
		console.log(e);
	}
}


function checkRotate(dg){
	let available = true;
	if(currentModel["type"]!="Block"){
		try{
			let middl = currentModel[ROTATE_INDEX];
			for(let i in currentModel){
				if(typeof(currentModel[i])=="object"){
					if(i!=ROTATE_INDEX){
						let x_backUP = (currentModel[i])[X_COO];
						let x_toCheck;
						let y_toCheck;
						if(dg<0){
							x_toCheck = (middl[Y_COO]-currentModel[i][Y_COO]) + middl[X_COO];
							y_toCheck = ((-1)*(middl[X_COO]-x_backUP)) + middl[Y_COO];
						}else{
							x_toCheck = ((-1)*(middl[Y_COO]-currentModel[i][Y_COO])) + middl[X_COO];
							y_toCheck = (middl[X_COO]-x_backUP) + + middl[Y_COO];
						}
						
						if(x_toCheck<0 || x_toCheck> CUBE_AMOUNT_X-1 || y_toCheck> CUBE_AMOUNT_Y-1){
							throw "Out of Bounds";
						}

						if(y_toCheck >= 0 && !tableModel[y_toCheck][x_toCheck].free){
							throw "No place: "+tableModel[y_toCheck][x_toCheck].td.style.backgroundColor;
						}
					}
				}
			}
		}catch(e){
			available = false;
			console.log(e);
		}
	}
	return available;
}

function rotate(dg){
	if(currentModel["type"]!="Block"){
		let middl = currentModel[ROTATE_INDEX];

		for(let i in currentModel){
			if(typeof(currentModel[i])=="object"){
				if(i!=ROTATE_INDEX){
					let x_backUP = (currentModel[i])[X_COO];
					if(dg<0){
						(currentModel[i])[X_COO] = (middl[Y_COO]-currentModel[i][Y_COO]) + middl[X_COO];
						(currentModel[i])[Y_COO] = ((-1)*(middl[X_COO]-x_backUP)) + middl[Y_COO];
					}else{
						(currentModel[i])[X_COO] = ((-1)*(middl[Y_COO]-currentModel[i][Y_COO])) + middl[X_COO];
						(currentModel[i])[Y_COO] = (middl[X_COO]-x_backUP) + + middl[Y_COO];
					}
				}
			}
		}
	}
}


/////////////////////////////////////////////

function sendDataToServer(info, ){
	let xhr = new XMLHttpRequest();
	let data = info;

	xhr.open("POST", data);
	xhr.setRequestHeader("Content-Type", "text/plain");
	xhr.onreadystatechange = function () {
 	 	if (xhr.readyState === 4) {
    			//console.log(xhr.status);
    			//console.log(xhr.responseText);
  	}};
	xhr.send(data);
}

function calcDelay() {
	return STANDARD_DELAY * 2 / (1 + stats.round);
}

function displayTopScores(scores) {
	let table = document.getElementById("scoreTable");
	//clear table
	while(table.firstChild) {
		table.removeChild(table.lastChild);
}

scores.forEach((score,idx) => {
	let tr = table.insertRow();
	let tdPos  = tr.insertCell();
	tdPos.innerHTML = idx+1 + ".";
	tdPos.style.padding = "0px 5px 0px 5px";
	let tdScore = tr.insertCell();
	tdScore.innerHTML = score.score;
	tdScore.style.padding = "0px 5px 0px 5px";
});
}

function getScoresFromServer() {
	let xhr = new XMLHttpRequest();
	xhr.onreadystatechange = onScoresLoaded;
	xhr.open('GET', '/api/score', true);
	xhr.send();
}

function onScoresLoaded()
{
	if(this.readyState == 4)
	{
		if(this.status >= 200 && this.status <= 399)
		{
		//console.log(this.responseText);
		displayTopScores(JSON.parse(this.responseText));
		}
	}
}
